﻿#pragma once

UENUM(BlueprintType)
enum EInteractionEventType : uint8
{
	InteractionStart = 0,
	InteractionEnd = 1,
};
